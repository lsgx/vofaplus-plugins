#ifndef DTTESTDATA_GLOBAL_H
#define DTTESTDATA_GLOBAL_H

#include "dllpublic_global.h"

#if defined(DTTESTDATA_LIBRARY)
#  define DTTESTDATA_EXPORT G_DECL_EXPORT
#else
#  define DTTESTDATA_EXPORT G_DECL_IMPORT
#endif

#endif // DTTESTDATA_GLOBAL_H
