#ifndef DTTESTDATA_H
#define DTTESTDATA_H

#include <QObject>

#include "dttestdata_global.h"

#include "dataengineinterface.h"

#define DtTestData_iid "VOFA+.Plugin.DtTestData/1.0"

#define DtTestData_imd "dttestdata.json"

class DTTESTDATA_EXPORT DtTestData : public QObject, public DataEngineInterface
{
    Q_OBJECT

    Q_PLUGIN_METADATA(IID DtTestData_iid FILE DtTestData_imd)

    Q_INTERFACES(DataEngineInterface)

public:
    explicit DtTestData(QObject *parent = nullptr);

    virtual ~DtTestData() override;

public:
    virtual void ProcessingDatas(char *data, int count) override;
};

#endif // DTTESTDATA_H
