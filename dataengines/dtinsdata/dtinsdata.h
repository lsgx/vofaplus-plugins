#ifndef DTINSDATA_H
#define DTINSDATA_H

#include <QObject>

#include "dtinsdata_global.h"

#include "dataengineinterface.h"

#define DtInsData_iid "VOFA+.Plugin.DtInsData/1.0"

#define DtInsData_imd "dtinsdata.json"

class DTINSDATA_EXPORT DtInsData : public QObject, public DataEngineInterface
{
    Q_OBJECT

    Q_PLUGIN_METADATA(IID DtInsData_iid FILE DtInsData_imd)

    Q_INTERFACES(DataEngineInterface)

public:
    explicit DtInsData(QObject *parent = nullptr);

    virtual ~DtInsData() override;

public:
    virtual void ProcessingDatas(char *data, int count) override;
};

#endif // DTINSDATA_H
