#ifndef DTINSDATA_GLOBAL_H
#define DTINSDATA_GLOBAL_H

#include "dllpublic_global.h"

#if defined(DTINSDATA_LIBRARY)
#  define DTINSDATA_EXPORT G_DECL_EXPORT
#else
#  define DTINSDATA_EXPORT G_DECL_IMPORT
#endif

#endif // DTINSDATA_GLOBAL_H
