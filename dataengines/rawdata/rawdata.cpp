#include "rawdata.h"

#include <QDebug>

#if defined(WIN32) && defined(_MSC_VER) &&  defined(_DEBUG)
     #define _CRTDBG_MAP_ALLOC
     #include <stdlib.h>
     #include <crtdbg.h>
     #define DEBUG_NEW new( _NORMAL_BLOCK, __FILE__, __LINE__ )
     #define new DEBUG_NEW
#endif

RawData::RawData(QObject *parent) :
    QObject(parent)
{
}

RawData::~RawData()
{
}

void RawData::ProcessingDatas(char *data, int count)
{
    Q_UNUSED(data)

    qDebug() << QString::fromUtf8("Plugin(%1), func(%2)").arg(QStringLiteral(RawData_iid)).arg(QStringLiteral(Q_FUNC_INFO));

    frame_list_.clear();

    // 将所有数据包含为一帧，is_valid_为false，表示这不是一个采样数据包、也不是一个图片数据包
    Frame frame;
    frame.start_index_ = 0;
    frame.end_index_ = count - 1;
    frame.is_valid_ = false;
    frame.image_size_ = 0;
    frame_list_.append(frame);
}
