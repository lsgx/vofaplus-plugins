#ifndef RAWDATA_GLOBAL_H
#define RAWDATA_GLOBAL_H

#include "dllpublic_global.h"

#if defined(RAWDATA_LIBRARY)
#  define RAWDATA_EXPORT G_DECL_EXPORT
#else
#  define RAWDATA_EXPORT G_DECL_IMPORT
#endif

#endif // RAWDATA_GLOBAL_H
