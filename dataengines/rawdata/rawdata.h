#ifndef RAWDATA_H
#define RAWDATA_H

#include <QObject>

#include "rawdata_global.h"

#include "dataengineinterface.h"

#define RawData_iid "VOFA+.Plugin.RawData/1.0"

#define RawData_imd "rawdata.json"

class RAWDATA_EXPORT RawData : public QObject, public DataEngineInterface
{
    Q_OBJECT

    Q_PLUGIN_METADATA(IID RawData_iid FILE RawData_imd)

    Q_INTERFACES(DataEngineInterface)

public:
    explicit RawData(QObject *parent = nullptr);

    virtual ~RawData() override;

public:
    virtual void ProcessingDatas(char *data, int count) override;
};

#endif // RAWDATA_H
