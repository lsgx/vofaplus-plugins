#ifndef DTIMUDATA_GLOBAL_H
#define DTIMUDATA_GLOBAL_H

#include "dllpublic_global.h"

#if defined(DTIMUDATA_LIBRARY)
#  define DTIMUDATA_EXPORT G_DECL_EXPORT
#else
#  define DTIMUDATA_EXPORT G_DECL_IMPORT
#endif

#endif // DTIMUDATA_GLOBAL_H
