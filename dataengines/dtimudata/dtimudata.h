#ifndef DTIMUDATA_H
#define DTIMUDATA_H

#include <QObject>

#include "dtimudata_global.h"

#include "dataengineinterface.h"

#define DtImuData_iid "VOFA+.Plugin.DtImuData/1.0"

#define DtImuData_imd "dtimudata.json"

class DTIMUDATA_EXPORT DtImuData : public QObject, public DataEngineInterface
{
    Q_OBJECT

    Q_PLUGIN_METADATA(IID DtImuData_iid FILE DtImuData_imd)

    Q_INTERFACES(DataEngineInterface)

public:
    explicit DtImuData(QObject *parent = nullptr);

    virtual ~DtImuData() override;

public:
    virtual void ProcessingDatas(char *data, int count) override;
};

#endif // DTIMUDATA_H
