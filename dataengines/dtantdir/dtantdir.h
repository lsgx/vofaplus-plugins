#ifndef DTANTDIR_H
#define DTANTDIR_H

#include <QObject>

#include "dtantdir_global.h"

#include "dataengineinterface.h"

#define DtAntDir_iid "VOFA+.Plugin.DtAntDir/1.0"

#define DtAntDir_imd "dtantdir.json"

class DTANTDIR_EXPORT DtAntDir : public QObject, public DataEngineInterface
{
    Q_OBJECT

    Q_PLUGIN_METADATA(IID DtAntDir_iid FILE DtAntDir_imd)

    Q_INTERFACES(DataEngineInterface)

public:
    explicit DtAntDir(QObject *parent = nullptr);

    virtual ~DtAntDir() override;

public:
    virtual void ProcessingDatas(char *data, int count) override;
};

#endif // DTANTDIR_H
