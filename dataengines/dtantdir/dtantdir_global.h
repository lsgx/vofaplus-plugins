#ifndef DTANTDIR_GLOBAL_H
#define DTANTDIR_GLOBAL_H

#include "dllpublic_global.h"

#if defined(DTANTDIR_LIBRARY)
#  define DTANTDIR_EXPORT G_DECL_EXPORT
#else
#  define DTANTDIR_EXPORT G_DECL_IMPORT
#endif

#endif // DTANTDIR_GLOBAL_H
