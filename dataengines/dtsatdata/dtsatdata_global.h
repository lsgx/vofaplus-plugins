#ifndef DTSATDATA_GLOBAL_H
#define DTSATDATA_GLOBAL_H

#include "dllpublic_global.h"

#if defined(DTSATDATA_LIBRARY)
#  define DTSATDATA_EXPORT G_DECL_EXPORT
#else
#  define DTSATDATA_EXPORT G_DECL_IMPORT
#endif

#endif // DTSATDATA_GLOBAL_H
