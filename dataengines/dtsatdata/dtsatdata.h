#ifndef DTSATDATA_H
#define DTSATDATA_H

#include <QObject>

#include "dtsatdata_global.h"

#include "dataengineinterface.h"

#define DtSatData_iid "VOFA+.Plugin.DtSatData/1.0"

#define DtSatData_imd "dtsatdata.json"

class DTSATDATA_EXPORT DtSatData : public QObject, public DataEngineInterface
{
    Q_OBJECT

    Q_PLUGIN_METADATA(IID DtSatData_iid FILE DtSatData_imd)

    Q_INTERFACES(DataEngineInterface)

public:
    explicit DtSatData(QObject *parent = nullptr);

    virtual ~DtSatData() override;

public:
    virtual void ProcessingDatas(char *data, int count) override;
};

#endif // DTSATDATA_H
