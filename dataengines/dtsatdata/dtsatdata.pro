TEMPLATE = lib
DEFINES += DTSATDATA_LIBRARY

QT_VER = $$[QT_VERSION]
QT_PATH = $$[QT_INSTALL_PREFIX]

#message(Qt version is $${QT_VER})
#message(Qt install prefix is $${QT_PATH})

QT       += core

QT       -= gui

#greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

#QT       += network

#QT       += xml

#QT       += testlib

#QT       += qml quick

#CONFIG += qmltestcase testcase

#CONFIG -= qt

#win32: CONFIG += console
macx: CONFIG -= app_bundle

CONFIG += debug_and_release

TARGET = dtsatdata
CONFIG(debug, debug|release) {
    unix: TARGET = $$join(TARGET,,,_debug)
    else: TARGET = $$join(TARGET,,,d)
}

# Common config for qmake.
include($${PWD}/../config.pri)

# Default rules for buildoutput.
include($${PWD}/../distout.pri)

# Default rules for deployment.
include($${PWD}/../deploy.pri)

# Custom DEFINES and CONFIG
DEFINES += PRINTF_LIBRARY

#message(C compiler preprocessor macros: $${DEFINES})
#message(C compiler flags: $${QMAKE_CFLAGS})
#message(C++ compiler flags: $${QMAKE_CXXFLAGS})
#message(linker flags: $${QMAKE_LFLAGS})

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =


# 多语言翻译文件
TRANSLATIONS +=

HEADERS += \
    dtsatdata_global.h \
    dtsatdata.h

SOURCES += \
    dtsatdata.cpp

FORMS +=

RESOURCES +=

OTHER_FILES += \
    $${PWD}/dtsatdata.json


# 子工程
#include($${PWD}/subproj/subproj.pri)


# 程序编译时依赖的相关路径
DEPENDPATH += \
    $${PWD}/../incs

# 头文件包含路径
INCLUDEPATH += \
    $${PWD}/../incs


win32 {
    WIN_JSON_IMD = $${PWD}/dtsatdata.json
    WIN_DEST_DIR = $${DESTDIR}

    msvc: WIN_JSON_IMD ~= s,/,\\,g
    msvc: WIN_DEST_DIR ~= s,/,\\,g

    QMAKE_POST_LINK += $$quote($${QMAKE_COPY} $${WIN_JSON_IMD} $${WIN_DEST_DIR}$$escape_expand(\n\t))
}

macos {
    APP_JSON_IMD.files = $$files($${PWD}/dtsatdata.json)
    APP_JSON_IMD.path = Contents/MetaData/
    QMAKE_BUNDLE_DATA += APP_JSON_IMD
}


######################
#win32 {
#    contains(QT_ARCH, x86_64) {
#        CONFIG(debug, debug|release) {
#            LIBS += $${PWD}/../libs/win/x64 -lmylibd
#        } else {
#            LIBS += $${PWD}/../libs/win/x64 -lmylib
#        }
#    } else {
#        CONFIG(debug, debug|release) {
#            LIBS += $${PWD}/../libs/win/x86 -lmylibd
#        } else {
#            LIBS += $${PWD}/../libs/win/x86 -lmylib
#        }
#    }
#}
#
#macos {
#    CONFIG(debug, debug|release) {
#        LIBS += $${PWD}/../libs/mac -lmylib_debug
#    } else {
#        LIBS += $${PWD}/../libs/mac -lmylib
#    }
#}
#
#linux {
#    CONFIG(debug, debug|release) {
#        LIBS += $${PWD}/../libs/linux -lmylib_debug
#    } else {
#        LIBS += $${PWD}/../libs/linux -lmylib
#    }
#}
######################
